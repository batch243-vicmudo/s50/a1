//import of the classes needed for the CRC rule as well as the classes needed for
import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlights p-3">
          {/*This is the first Card*/}
          <Card.Body>
            <Card.Title>
              <h2>Learn From Home</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ac
              vestibulum sem. Sed dictum tortor nec ligula vehicula facilisis.
              Morbi quis nulla dui. Nam sit amet dui ut metus accumsan posuere a
              id ipsum. Praesent risus lorem, tempus sed vestibulum eu, vehicula
              nec eros.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      {/*Second Card*/}
      <Col xs={12} md={4}>
        <Card className="cardHighlights p-3">
          <Card.Body>
            <Card.Title>
              <h2>Study Now, Pay Later</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ac
              vestibulum sem. Sed dictum tortor nec ligula vehicula facilisis.
              Morbi quis nulla dui. Nam sit amet dui ut metus accumsan posuere a
              id ipsum. Praesent risus lorem, tempus sed vestibulum eu, vehicula
              nec eros.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      {/*This is third card*/}
      <Col xs={12} md={4}>
        <Card className="cardHighlights p-3">
          <Card.Body>
            <Card.Title>
              <h2>Be part of our community</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ac
              vestibulum sem. Sed dictum tortor nec ligula vehicula facilisis.
              Morbi quis nulla dui. Nam sit amet dui ut metus accumsan posuere a
              id ipsum. Praesent risus lorem, tempus sed vestibulum eu, vehicula
              nec eros.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
