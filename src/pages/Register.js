import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

import { Container, Row, Col } from "react-bootstrap";

//we use this to get the input of the user
import { useState, useEffect, useContext } from "react";

import Swal from "sweetalert2";

import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function Register() {
  //state hooks to store the values of the input field from our user
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");

  const [isActive, setIsActive] = useState(false);

  const { user, setUser } = useContext(UserContext);

  /*Business Logic*/
  // we want to disable the register button if one of the input fields is empty

  useEffect(() => {
    if (
      email !== "" &&
      password !== "" &&
      password2 !== "" &&
      password === password2 &&
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== "" &&
      mobileNo.length <= 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, password2, firstName, lastName, mobileNo]);

  //register features
  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_URI}/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        mobileNo: mobileNo,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data.emailExists) {
          Swal.fire({
            title: "Email Already Exists",
            icon: "error",
            text: "Please try another email!",
          });
        } else if (user) {
          Swal.fire({
            title: "Registration Complete",
            icon: "success",
            text: "Welcome to our website!",
          });

          fetch(`${process.env.REACT_APP_URI}/users/login`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: email,
              password: password,
            }),
          })
            .then((response) => response.json())
            .then((data) => {
              console.log(data);

              if (data.accessToken !== "empty") {
                localStorage.setItem("token", data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                  title: "Registration Complete",
                  icon: "success",
                  text: "Welcome to our website!",
                });
              } else {
                Swal.fire({
                  title: "Authentication failed!",
                  icon: "error",
                  text: "Check the information you enter and try again.",
                });

                setPassword("");
              }
            });

          const retrieveUserDetails = (token) => {
            fetch(`${process.env.REACT_APP_URI}/users/profile`, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
              .then((response) => response.json())
              .then((data) => {
                console.log(data);

                setUser({ id: data._id, isAdmin: data.isAdmin });
              });
          };
        } else {
          Swal.fire({
            title: "Authentication failed!",
            icon: "error",
            text: "Check the information you enter and try again.",
          });
        }
      });

    // fetch(`${process.env.REACT_APP_URI}/users/login`, {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify({
    //     email: email,
    //     password: password,
    //   }),
    // })
    //   .then((response) => response.json())
    //   .then((data) => {
    //     console.log(data);

    //     if (data.accessToken !== "empty") {
    //       localStorage.setItem("token", data.accessToken);
    //       retrieveUserDetails(data.accessToken);

    //       Swal.fire({
    //         title: "Registration Complete",
    //         icon: "success",
    //         text: "Welcome to our website!",
    //       });
    //     } else {
    //       Swal.fire({
    //         title: "Authentication failed!",
    //         icon: "error",
    //         text: "Check the information you enter and try again.",
    //       });

    //       setPassword("");
    //     }
    //   });

    // const retrieveUserDetails = (token) => {
    //   fetch(`${process.env.REACT_APP_URI}/users/profile`, {
    //     headers: {
    //       Authorization: `Bearer ${token}`,
    //     },
    //   })
    //     .then((response) => response.json())
    //     .then((data) => {
    //       console.log(data);

    //       setUser({ id: data._id, isAdmin: data.isAdmin });
    //     });
    // };
  }

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Row>
        <Col className="col-md-4 col-8 offset-md-4 offset-2">
          <Form onSubmit={registerUser} className="bg-info p-3">
            <Form.Group className="mb-3" controlId="firstName">
              <Form.Label>Enter your First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="FirstName"
                value={firstName}
                onChange={(event) => setFirstName(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
              <Form.Label>Enter your Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="LastName"
                value={lastName}
                onChange={(event) => setLastName(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Enter your desired Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
              <Form.Label>Enter your desired Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password2}
                onChange={(event) => setPassword2(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
              <Form.Label>Enter your Mobile Number</Form.Label>
              <Form.Control
                type="text"
                placeholder="MobileNumber"
                value={mobileNo}
                onChange={(event) => setMobileNo(event.target.value)}
                required
              />
            </Form.Group>

            <Button variant="primary" type="submit" disabled={!isActive}>
              Register
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
