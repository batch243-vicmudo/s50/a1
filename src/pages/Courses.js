import { Fragment, useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";
//import courseData from "../data/courses.js";

export default function Course() {
  const [courses, setCourses] = useState([]);
  /*console.log(courseData);*/

  //Getting localstorage
  //Syntax: localStorage.getItem("propertyName")

  // const local = localStorage.getItem("email");
  // console.log(local);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );
      });
  }, []);

  return <Fragment>{courses}</Fragment>;
}
